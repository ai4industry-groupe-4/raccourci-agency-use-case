# Jeu de données

Ce jeu de données se compose d'enregistrements issus de feuillets roadbook et de fiches SIT.

Les feuillets roadbook sont des documents rédigés par des conseillers en séjour.
Ils présentent une activité ou une liste d'activités à destination des voyageurs.
Quant aux fiches SIT, elles fournissent des informations détaillées sur différents établissements ou activités.

Le jeu de données comprend deux fichiers principaux : `roadbook.json` et `sit.json`.
Ces fichiers sont structurés au format JSON Lines et contiennent chacun 8 champs distincts :

- `id` : Identifiant unique de l'enregistrement.
- `title` : Titre du contenu, correspondant au nom de l'établissement ou de l'activité.
- `content` : Texte descriptif présentant l'établissement ou l'activité.
- `description` : Brève description de l'établissement ou de l'activité.
- `bordereau` : Catégorie de l'établissement (voir liste pour les détails).
- `website` : Site web officiel de l'établissement ou de l'activité.
- `fairguest_rating` : Note attribuée par FairGuest, permettant d'évaluer la satisfaction des clients.
- `source` : Origine de l'enregistrement, indiquant s'il provient d'un roadbook ou d'une fiche SIT.

# Bordereaux

| Type                                                           | Code  |
|----------------------------------------------------------------|-------|
| Hébergements collectifs                                        | HCO   |
| Hébergements locatifs                                          | HLO   |
| Hôtellerie                                                     | HOT   |
| Hôtellerie de plein air                                        | HPA   |
| Résidences de Tourisme                                         | RETOU |
| Villages Vacances classés                                      | VIL   |
| Activités sportives, activités culturelles, séjours itinérants | ASC   |
| Dégustations                                                   | DEG   |
| Itinéraires touristiques                                       | ITI   |
| Activités et équipements de loisirs                            | LOI   |
| Patrimoine culturel                                            | PCU   |
| Patrimoine naturel                                             | PNA   |
| Restauration                                                   | RES   |
| Fêtes et manifestations                                        | FMA   |
| Organismes et entreprises                                      | ORG   |

# Validation

Le fichier `validation.json` constitue un jeu de données conçu pour la vérification des hypothèses de recherche. Il est
structuré au format JSON et intègre deux champs :

- `question` : Formulation de la question posée par l'utilisateur.
- `responses` : Liste des identifiants des réponses potentielles.

## Description des questions

La question "**Que faire sur l'île de Ré avec des enfants quand il pleut ?**" cherche des suggestions d'activités
adaptées aux enfants à réaliser sur l'île de Ré, lorsqu'il fait mauvais temps.
Cette question sous-entend que le temps pluvieux limite les activités habituelles en
extérieur, comme la plage ou les balades, et demande donc des idées pour des activités intérieures ou des choses à faire
qui sont adaptées aux jours de pluie. Cela peut inclure des visites de musées, des ateliers créatifs, des espaces de
jeux couverts, des cinémas, des activités éducatives ou des lieux de divertissement adaptés aux familles et aux enfants.

La question "**Sur quelle plage puis-je promener mon chien ?**" cherche à obtenir des informations sur les plages qui
autorisent la présence de chiens. La personne reconnaît que certaines plages ont des réglementations spécifiques
concernant les animaux de compagnie, en particulier les chiens. La personne s'intéresse à trouver un lieu où son chien
pourra se promener confortablement et en toute sécurité, sans risque de perturber l'écosystème de la plage ou de causer
des désagréments aux autres usagers.

La question "**Quel est le meilleur restaurant de spécialité italienne ?**" cherche à identifier le restaurant offrant
la meilleure expérience culinaire en matière de cuisine italienne.
Elle reflète un intérêt spécifique pour la cuisine italienne, ce qui peut indiquer une appréciation pour les plats
tels que les pâtes, les pizzas, les risottos, et autres spécialités italiennes.

La question "**Quels sont les horaires des marchés à côté de la Flotte en Ré ?**" cherche à obtenir des informations
spécifiques sur les horaires d'ouverture des marchés situés près de La Flotte en Ré. Cela souligne un intérêt pour les
activités et expériences locales lors d'un séjour dans cette région.

La question "**Quelles sont les sorties bateaux ?**" demande des informations sur les possibilités de sorties en bateau
disponibles. La question peut faire référence à différents types de sorties en bateau, comme des excursions 
touristiques, des sorties de pêche, des croisières, des tours en voilier, ou même des locations de bateau
pour des sorties individuelles.
La personne cherche des informations pratiques telles que les horaires, les lieux de départ, la durée des sorties, les
coûts, et peut-être les conditions requises pour participer (par exemple, réservations nécessaires, équipement fourni,
niveau d'expérience requis)

La question "**Quelles sont les activités incontournables avec enfants ?**" demande des recommandations sur des
activités particulièrement adaptées ou attrayantes pour les enfants.
La question indique le désir de trouver des activités qui peuvent être appréciées en famille, permettant aux parents et
aux enfants de passer du temps de qualité ensemble.
L'adjectif "incontournables" suggère un intérêt pour des activités qui sont particulièrement populaires ou hautement
recommandées, souvent considérées comme des expériences essentielles dans un lieu donné.

La question "**Quelles sont les activités incontournables sans enfants ?**" demande des recommandations sur des
activités qui sont particulièrement adaptées pour les adultes ou qui sont mieux appréciées sans la présence d'enfants.
Une recherche d'activités plus calmes, relaxantes ou sophistiquées, comme des dégustations de vins, des spectacles de
théâtre, des expositions d'art, des spas, ou des dîners gastronomiques.

La question "**Comment occuper mes enfants ?**" cherche des idées ou des suggestions pour engager et divertir les
enfants.
Cette question vise à obtenir des idées pour occuper les enfants de manière agréable et potentiellement éducative, en
tenant compte de leurs intérêts, de leur âge, sans pour autant impliquer les parents dans l'activité.

La question "**Quels sont les musées à voir ?**" demande des recommandations sur les musées jugés intéressants ou
incontournables.
La personne cherche des conseils sur les musées qui sont particulièrement remarquables, soit en raison de leurs
collections, de leur architecture, de leur importance historique, ou de leur pertinence culturelle.

La question "**Quels sont les produits locaux à tester ?**" cherche des recommandations sur les spécialités culinaires
ou des produits caractéristiques de la région.
La question suggère une volonté d'explorer la diversité des saveurs et des pratiques culinaires propres à un lieu,
offrant une expérience gustative authentique.
Un désir d'expérimenter la culture d'une région, en comprenant mieux son histoire, son terroir, et ses traditions.

# Déclinaison des questions
Une même question peut-^tre formulée de façon differentes, nous prposons ici troi variantes de chacune des questions.

**Que faire sur l'île de Ré avec des enfants quand il pleut ?**
 - Quoi faire île de Ré avec enfants quand pluie ?
 - Besoin d'idées pour occuper les enfants sur l'île de Ré quand il pleut.
 - Quelles activités intérieures sont recommandées pour les enfants sur l'île de Ré par temps de pluie ?

**Sur quelle plage puis-je promener mon chien ?**
- Où plage pour promenade chien ? 
- Je cherche une plage pour promener mon chien. 
- Quelles plages de l'île de Ré autorisent les promenades avec un chien ?

**Quel est le meilleur restaurant de spécialité italienne ?**
 - Meilleur restaurant italien où c'est ?
 - Je veux savoir le meilleur restaurant italien.
 - Où puis-je trouver le restaurant italien le plus réputé ?

**Quels sont les horaires des marchés à côté de la Flotte en Ré ?**
 - Horaire marché près Flotte en Ré quel est ?
 - Besoin horaires des marchés près de la Flotte en Ré.
 - À quelles heures peut-on visiter les marchés près de la Flotte en Ré ?

**Quelles sont les sorties bateaux ?**
 - Sortie bateau quelles sont ?
 - Voudrais liste des sorties en bateau.
 - Quels sont les tours en bateau disponibles ?

**Quelles sont les activités incontournables avec enfants ?**
 - Activité important avec enfants quoi ?
 - Cherche activités à faire avec des enfants.
 - Quelles sont les meilleures activités à faire avec des enfants ?

**Quelles sont les activités incontournables sans enfants ?**
 - Activité faire sans enfants quoi bon ?
 - Je veux savoir les activités pour adultes sans enfants.
 - Quelles activités sont idéales pour les adultes sans enfants ?

**Comment occuper mes enfants ?**
 - Comment faire occuper enfants ?
 - Besoin idées pour occuper mes enfants.
 - Quelles activités ludiques puis-je proposer à mes enfants ?

**Quels sont les musées à voir ?**
 - Musée bon à voir ?
 - Je recherche les musées intéressants à visiter.
 - Quels musées recommandez-vous de visiter ?

**Quels sont les produits locaux à tester ?**
 - Produit local bon pour goûter quoi ?
 - Je cherche à connaître les produits locaux à essayer.
 - Quels produits régionaux devrais-je essayer ?